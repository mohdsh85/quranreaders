<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use CodeIgniter\Model;
/**
 * Description of clientModel
 *
 * @author Mobile Development
 */
class filesModel extends Model {
    protected $table = 'uploaded_files';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id', 'client_id','age_range','gender','uploaded_date','inner_path','total_likes','total_comments','total_share','file_name','uploaded_by'];
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
}
