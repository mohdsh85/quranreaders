<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use CodeIgniter\Model;
/**
 * Description of clientModel
 *
 * @author Mobile Development
 */
class clientModel extends Model {
    protected $table = 'clients';
    protected $primaryKey = 'client_id';
    protected $allowedFields = ['name', 'email','age_range','gender','register_date','verify_code','is_verified','client_id','client_password'];
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
}
